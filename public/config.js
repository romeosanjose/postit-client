prod = {
    url : {
        API_URL : "http://192.168.57.107"
    }
};

const dev = {
    url : {
        API_URL : 'http://postit.local'
    }
};

export const config = process.env.NODE_ENV === 'production' ? dev : prod;