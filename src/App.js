import React, {Component} from 'react';
import Navbar from './NavBar/NavBar';
import MyProfile from './User/MyProfile';
import Posts from './Posts/Posts';
import {Link} from 'react-router-dom';
import Treatments from './Treatment/Treatments';


class App extends Component{
  
  constructor(props){
    super(props);
    if (!localStorage.getItem('user')) {
        this.props.history.push('/login');
    } 
    let user = JSON.parse(localStorage.getItem('user'));
    this.state = {
        user : user,
        api_url: process.env.REACT_APP_API_URL,
    };
}

  render(){
    
    var pageContainerStyle={
      maxWidth: "1400px",
      marginTop: "80px"
    }

    return(
        <div>
            <Navbar />

            {/* Page Container */}
            <div className="w3-container w3-content" style={pageContainerStyle} >    
                {/* The Grid */}
                <div className="w3-row">
                    {/* Left Column  */}
                  <div className="w3-col m3">
                      <MyProfile />
                      <br></br>

                      
                      <br></br>

                      {/* Alert Box
                      <div className="w3-container w3-display-container w3-round w3-theme-l4 w3-border w3-theme-border w3-margin-bottom w3-hide-small">
                        <span onclick="this.parentElement.style.display='none'" className="w3-button w3-theme-l3 w3-display-topright">
                          <i className="fa fa-remove"></i>
                        </span>
                        <p><strong>Hey!</strong></p>
                        <p>People are looking at your profile. Find out who.</p>
                      </div> */}
                   {/* End Left Column */}
                  </div>

                  {/* Middle Column */}
                  <div className="w3-col m7">
                     
                          
                      <Posts />                     
                  {/* End Middle Column  */}
                  </div>
                  {/* Right Column */}
                  <div className="w3-col m2">
                      <div className="w3-card w3-round w3-white w3-center">
                          <div className="w3-container">
                            <p>Treatments</p>
                            <Treatments />
                          </div>
                      </div>
                      <br></br>

                      
                    
                  {/* End Right Column */}
                  </div>
                {/* End Grid */}
                </div>
            {/* End of COntainer */}
            </div>
            
             {/* Footer */}
            <footer classname="w3-container w3-theme-d3 w3-padding-16">
            </footer>
            <footer className="w3-container w3-theme-d5">
              
            </footer>
      </div>
    );
  }
}


export default App;



