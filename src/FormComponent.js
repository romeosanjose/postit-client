import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import SideBar from './NavBar/Admin/SideBar';

class FormComponent extends Component{

    constructor(props) {
        super(props);
        let currentUser = JSON.parse(localStorage.getItem('user'));
        this.state = {
            api_url: process.env.REACT_APP_API_URL,
            currentUser : currentUser
        }
    }

    handleCatch = (error) => {
        if (error.response) {
            console.log(error.response.data);
            let errorData = error.response.data;
            const errors = (Array.isArray(errorData))   
                ? error.response.data.map((item, key)=>
                    <p>{item}<br/></p>
                ) : <p>{error.response.data}</p>;
            this.setState({message : errors});   
        } else if (error.request) {
            console.log('request error', error.request);
            this.setState({message : 'Its not you, its me'});    
        } else {
            console.log('error message', error.message);
            this.setState({message : 'Its not you, its me'});    
        }
    }

    render(){
        return(
            <div>
                {/* Page Wrapper */}
                <div id="wrapper">
                    <SideBar />
                    {/* Content Wrapper */}
                    <div id="content-wrapper" className="d-flex flex-column">
                        {/* Main Content */}
                        <div id="content">
                            {this.loadForm()}
                        {/* End of main Content */}
                        </div>
                    {/* End of Content Wrapper */}
                    </div>
                {/* End - Page Wrapper */}
                </div>  
            </div>
        );
    }
}

export default FormComponent;