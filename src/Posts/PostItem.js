import React, {Component} from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';
import NavBar from '../NavBar/NavBar';
import {Link} from 'react-router-dom';

class PostItem extends Component{
    
    constructor(props) {
        super(props);
        let user = JSON.parse(localStorage.getItem('user'));
        this.state = {
            user : user,
            created_by :  null,
            api_url: process.env.REACT_APP_API_URL,
            post : this.props.post
        };

        this.loadCreatedBy(this.props.post.created_by)
    }

    options() {
        return {
            headers : {
                'api-key' : (this.state.user) ? this.state.user.api_key : null
            }
        };
    }

    handleAccept(uuid){
        axios.patch(this.state.api_url + '/post/' + uuid, {}, this.options())
        .then(res => {
            if (res.data) {
                this.refreshPost(uuid);
            }
        });
        
    }

    refreshPost(uuid) {
        axios.get(this.state.api_url + '/post/' + uuid, this.options())
        .then(res => {
            const post = res.data;
            const element = (  post.assigned_to.uuid === this.state.user.uuid 
                            || post.created_by.uuid === this.state.user.uuid
                            || this.state.user.type === 'ADMIN') 
                            ? this.fullDetails(post) : this.acceptedDetails(post);
            ReactDOM.render(element, document.getElementById('post-' + uuid));
        });
    }

    postDetails(post){
        
        if (post.status === 'ACCEPTED') {
            if ( post.assigned_to.uuid === this.state.user.uuid 
                 || post.created_by === this.state.user.id
                 || this.state.user.type === 'ADMIN'
                ){
                return this.fullDetails(post);
            }
            return this.acceptedDetails(post);
        }
        let detailed_case_job = null;
        if (post.detailed_case_job) {
            detailed_case_job = <p><b>Detailed Treatment:</b> {post.detailed_case_job}</p>
        }
        return (
            <div>
                <p><b>Treatment:</b> {post.case_job}</p>
                {detailed_case_job}
                <button id={'post-accept-' + post.uuid} className="w3-button w3-theme-d1 w3-margin-bottom" onClick={() => this.handleAccept(post.uuid)}>
                    <i className="fa fa-thumbs-up"></i>Accept
                </button>
            </div>
        );
   }

   fullDetails(post) {
        return  (
            <div>
                <p><b>Treatment:</b> {post.case_job}</p>
                <p><b>Detailed Treatment:</b> {post.detailed_case_job}</p>
                <p><b>Patient ID: </b> {post.patient.id}</p>
                <p><b>Patient Age: </b> {post.patient.age}</p>
                <p><b>Patient Gender: </b> {post.patient.gender}</p>
                <p><b>Medical History: </b> {post.patient.background}</p>
                <p><b>Clinical Department Origin: </b> {post.patient.clinical_department_origin}</p>
                <p><b>Clinical Department Destination: </b> {post.patient.clinical_department_destination}</p>
                <p><b>Special Indications: </b> {post.patient.special_indications}</p>  
            </div>   
        );
    }

    acceptedDetails(post) {
        return (
            <div>
                <p><b>Treatment:</b> {post.case_job}</p>
                <p><b>Detailed Treatment:</b> {post.detailed_case_job}</p>
                <p><b>Accepted By: </b> {post.assigned_to.person.first_name + + post.assigned_to.person.last_name}</p>
            </div>   
        );
    }

    loadCreatedBy(userId){     
        
        const user = axios.get(this.state.api_url + '/user/' + userId, this.options())
        .then(res => {
            this.setState({
                created_by : res.data
            })
        }).catch(error => {
            this.handleCatch(error);
        })
       
     }


    

    render(){
        let post = this.state.post
        let avatar = (this.state.created_by && this.state.created_by.person.profile_picture) 
                    ? this.state.created_by.person.profile_picture.path
                    : '/profile_picture/avatar.jpg';
        let editBtn = (this.state.user.type === 'ADMIN') 
                ?   <Link to={{ pathname: "/admin/post/edit",  state : {post : post}}} >
                        <i className="fas fa-edit"></i>
                    </Link>
                : null;
        return (
            <div className="w3-container w3-card w3-white w3-round w3-margin">
                <br></br>
                <img src={this.state.api_url + '/' + avatar } alt="Avatar" className="w3-left w3-circle w3-margin-right" style={{width:"60px", height:"60px"}} />
                <span className="w3-right w3-opacity">
                    {editBtn}
                </span>
                {/* <a href='#'><h4>{this.state.created_by.person.first_name +' '+ this.state.created_by.person.last_name}</h4></a> */}
                <br></br>
                <hr className="w3-clear"></hr>
                <div id={'post-' + post.uuid}>
                    {this.postDetails(post)}
                </div>
            </div>
            
        );
    }
}


export default PostItem;