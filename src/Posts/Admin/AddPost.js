import React, {Component} from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';
import SideBar from '../../NavBar/Admin/SideBar';
import FormComponent from '../../FormComponent';

class AddPost extends FormComponent{

    constructor(props) {
        super(props);
        let user = JSON.parse(localStorage.getItem('user'));
        let post = {
            case_job: 'NONE',
            detailed_case_job: '',
            patient : {
                id: '',
                first_name : '',
                last_name: '',
                age : '',
                gender : 'MALE',
                background: '',
                clinical_department_origin : '',
                clinical_department_destination : '',
                chart_number : '',
                special_indications : '',
            }
        };
        this.state = {
            user : user, 
            selectedPatientData: 'new_patient',
            post: post,
            message: "",
            api_url : process.env.REACT_APP_API_URL,
            case_job_opts : [],
            show_detailed_case_job : false
        }
    }

    options(){
        return { 
            headers : { 
                'api-key' : this.state.user.api_key,
                'Content-Type' : 'application/json',
                'Accept' : 'application/json' 
            }
        }
    }

    handleTreatmentChange = (event) => {
        const select = event.target
        const post = this.state.post
        post[select.name] = select.value
        
        this.setState({
            post : post,
            show_detailed_case_job: parseInt(select.value) === 35            
        });        
      
    }

    showDetailedCaseJob(){
        return  <div className="col-sm-9 ">
                    <div className="form-group" id="detailed_case_job_wrapper" >
                        <textarea name="detailed_case_job" id="detailed-case-job" className="form-control"
                        value={this.state.post.detailed_case_job} onChange={this.handleChange} placeholder="detailed treatment"></textarea>
                    </div>
                </div>
    }

    handleChange = (event) => {
        const input = event.target;
        const post  = this.state.post;
        if (input.name === 'case_job') {
            post.case_job = input.value;
        } else if (input.name === 'detailed_case_job') {
            post.detailed_case_job = input.value;
        } else {
            post.patient[input.name] = input.value;
        }
        this.setState({ post : post })
    };

    handlePatientTypeChange = (event) => {
        this.setState({
            selectedPatientData : event.target.value
        });
        if (event.target.value === 'existing_patient') {
            this.loadPatientSelections();
        } else {
            ReactDOM.render(null, document.getElementById('patient-selection'));
        }
    }

    

    handlePatientSelect = (event) => {
        const patientId = event.target.value;
        axios.get(this.state.api_url + '/patient/' + patientId, this.options())
        .then(res => {
            const patient = res.data;
            const post = this.state.post;
            post.patient = patient;
            this.setState({
                post : post
            });
        });
    }

    handleGenderSelect = (event) => {
        const gender = event.target.value;
        const post = this.state.post;
        post.patient.gender = gender;
        this.setState({
            post : post
        });
    }

    loadPatientSelections () {
        axios.get(this.state.api_url + '/patient', this.options())
        .then(res => {
            const patientObjs = res.data;
            const element = this.buildPatientSelections(patientObjs);
            ReactDOM.render(element, document.getElementById('patient-selection'));
        });
    }

 
    buildPatientSelections(patientObjs) {
        return  <select onChange={this.handlePatientSelect} className="form-control">
                    { patientObjs.map(patient => (
                            <option value={patient.id}>{patient.id + ' - ' + patient.last_name + ', ' + patient.first_name}</option>
                        ))
                    }
                </select> 
    }

    handleFormSubmit = (event) => {
        event.preventDefault();
        const data = {
            post : this.state.post
        }
        const post = axios.post(this.state.api_url + '/post', data, this.options())
            .then(res => {
                this.props.history.push('/');
            })
            .catch(error => {
                if (error.response) {
                    this.setState({message : error.response.data});    
                } else if (error.request) {
                    console.log('request error', error.request);
                    this.setState({message : 'Its not you, its me'});    
                } else {
                    console.log('error message', error.message);
                    this.setState({message : 'Its not you, its me'});    
                }
            })
        
    }
    
    componentDidMount() {
        this.loadCaseJobTreatment();
    }

    loadCaseJobTreatment() {
        const case_job = axios.get(this.state.api_url + '/managelist/TREATMENT', this.options())
        .then(res => {
            this.setState({case_job_opts : res.data})
        }).catch(error => {
            this.handleCatch(error);
        })
    }

    loadForm(){
        return (
            <div>
                <div className="card o-hidden border-0 shadow-lg my-5">
                    <div className="card-body p-0">    
                        <div className="row">               
                            <div className="col-lg-5 d-none d-lg-block bg-register-image"></div>
                            <div className="col-lg-7">
                                <div className="p-5">
                                    <div className="text-center">
                                        <h1 className="h4 text-gray-900 mb-4">Create New Post</h1>
                                    </div>
                                    <form onSubmit={this.handleFormSubmit}>
                                        <div name="message" className="error-message">{this.state.message}</div>
                                        <div className="form-group row">
                                            <div className="col-sm-9 ">
                                                <div className="form-group">
                                                    <span style={{color:'grey', fontSize: '.8em'}}>* Treatment</span> 
                                                    <select onChange={this.handleTreatmentChange} name="case_job" id="case_job" className="select-css" required>
                                                    {
                                                        this.state.case_job_opts.map((case_job)=> <option key={case_job.id} value={case_job.id}>{case_job.name}</option>)
                                                    }
                                                    </select>
                                                </div>
                                            </div>
                                            {this.state.show_detailed_case_job ? this.showDetailedCaseJob() : null}
                                            <div className="col-sm-9 ">
                                                <div className="form-group">
                                                    <span style={{color:'grey', fontSize: '.8em'}}>* Patient</span> 
                                                    <select onChange={this.handlePatientTypeChange} id="patient_type" className="select-css" required>
                                                        <option value="new_patient">New Patient</option>
                                                        <option value="existing_patient">Existing Patient</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col-sm-9 ">
                                                <div className="form-group">
                                                    <div id="patient-selection"></div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="patient-details" className="col-sm-9 ">
                                            <div className="form-group">
                                                <input name="id" id="patient-id" className="form-control"
                                                value={this.state.post.patient.id} onChange={this.handleChange} placeholder="chart number *" required/>
                                            </div>
                                            <div className="form-group">
                                                <input name="age" id="age" className="form-control"
                                                value={this.state.post.patient.age} onChange={this.handleChange} placeholder="age"/>
                                            </div>
                                            <div className="form-group">
                                                <select onChange={this.handleGenderSelect} name="gender" id="gender" className="form-control">
                                                    <option value="MALE">Male</option>
                                                    <option value="FEMALE">Female</option>
                                                </select> 
                                            </div>
                                            <div className="form-group">
                                                <textarea name="background" id="background" className="form-control"
                                                value={this.state.post.patient.background} onChange={this.handleChange} placeholder="medical alert"></textarea>
                                            </div>
                                            <div className="form-group">
                                                <input name="clinical_department_origin" id="clinical-department-origin" className="form-control"
                                                value={this.state.post.patient.clinical_department_origin} onChange={this.handleChange} placeholder="Clinical Department Origin"/>
                                            </div>
                                            <div className="form-group">
                                                <input name="clinical_department_destination" id="clinical-department-destination" className="form-control"
                                                value={this.state.post.patient.clinical_department_destination} onChange={this.handleChange} placeholder="Clinical Department Destination"/>
                                            </div>
                                            <div className="form-group">
                                                <input name="special_indications" id="special-indications" className="form-control"
                                                value={this.state.post.patient.special_indications} onChange={this.handleChange} placeholder="Special Indications"/>
                                            </div>
                                        </div>
                                        <div>
                                            <button type="submit" className="btn btn-facebook btn-user btn-block">publish</button>
                                        </div>                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
            </div>
        );
    }



}

export default AddPost