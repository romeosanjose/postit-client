import React, {Component} from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';
import NavBar from '../NavBar/NavBar';
import {Link} from 'react-router-dom';
import PostItem from './PostItem';



class Posts extends Component{

   constructor(props) {
        super(props);
        let user = JSON.parse(localStorage.getItem('user'));
        this.state = {
            originalPosts : null,
            posts: null,
            post: null,
            user : user,
            api_url: process.env.REACT_APP_API_URL,
        }

        
    }

    options() {
        return {
            headers : {
                'api-key' : (this.state.user) ? this.state.user.api_key : null
            }
        };
    }

   async componentDidMount(){     
       const posts = (await axios.get(this.state.api_url + '/post', this.options())).data;
       this.setState({
            posts:posts,
            originalPosts : posts
       });
    }

    handleSearch =  (event) => {
        let searcjQery = event.target.value.toLowerCase()
        if (searcjQery === ''){
            this.setState({
                posts: this.state.originalPosts
            })
            return;    
        }
        
        let displayedPost = this.state.posts.filter((post) => {
                let searchValue = post.case_job.toString().toLowerCase();
                return searchValue.indexOf(searcjQery) !== -1;
            })
        this.setState({
            posts: displayedPost
        })
    }

    updatePosts(posts){
        this.setState({'posts' : posts})
    }

   
   render(){
       let posts = this.state.posts;
       return (
            <div>
                 <div className="w3-row-padding">
                    <div className="w3-col m12">
                        <div className="w3-container w3-padding">
                            <input type="text" className="form-control col-sm-12" onChange={this.handleSearch} placeholder="search..." />
                        </div>
                    </div>
                </div>
                <div className="w3-row-padding">
                    {/* new post */}
                    <div className="w3-col m12">
                    <div className="w3-card w3-round w3-white">
                        <div className="w3-container w3-padding">
                            <Link to="/admin/post/add" className="w3-button w3-theme">
                                <i className="fas fa-pencil-square"></i>  Add Post
                            </Link>
                        </div>
                    </div>
                    {/* End of new post */}
                    </div>
                </div>
                {posts === null && <img clasName="spinner" src="../img/spinner.gif" style={{marginLeft:"30%", maxHeight:"200px", maxWidth: "200px", height: "auto", width: "auto"}}/>} 
                <div>
                    {
                        posts && posts.map(post => (  
                            <PostItem post={post} />
                        ))
                    }
                </div>
            </div>
            
       );
   }
}

export default Posts;