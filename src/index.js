import React from 'react';
import ReactDOM from 'react-dom';
import {Route, Link, BrowserRouter as Router} from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import App from './App';
import Login from './Authentication/Login';
// admin
import Admin from './Admin';
import AddUser from './User/Admin/AddUser';
import UserList from './User/Admin/UserList';
import EditUser from './User/Admin/EditUser';

import AddPost from './Posts/Admin/AddPost';
import EditPost from './Posts/Admin/EditPost';
import ChangePassword from './Authentication/ChangePassword';
import ForgotPassword from './Authentication/ForgotPassword';

const routing = (
    <Router >
        <div>
            {/* admin */}
            <Route exact path="/admin" component={Admin} />    
            <Route exact path="/admin/user" component={UserList} />
            <Route exact path="/admin/user/add" component={AddUser} />
            <Route exact path="/admin/user/edit" component={EditUser} />

            <Route exact path="/admin/post/add" component={AddPost} />
            <Route exact path="/admin/post/edit" component={EditPost} />
            {/* end of admin */}
            <Route path="/login" component={Login} />
            <Route path="/password/change/:id" component={ChangePassword} />
            <Route path="/password/forgot" component={ForgotPassword} />
            <Route exact path="/" component={App} />
        </div>
    </Router>
)

ReactDOM.render(
    routing, 
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
