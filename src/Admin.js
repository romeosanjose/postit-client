import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import SideBar from './NavBar/Admin/SideBar';
import SideToggleBar from './NavBar/Admin/SideToggleBar';



class Admin extends Component{
  
  render(){
      return(
        <div>
            {/* Page Wrapper */}
            <div id="wrapper">
                <SideBar />
                {/* Content Wrapper */}
                <div id="content-wrapper" className="d-flex flex-column">
                    {/* Main Content */}
                    <div id="content">
                        
                    {/* End of main Content */}
                    </div>
                {/* End of Content Wrapper */}
                </div>
            {/* End - Page Wrapper */}
            </div>  
        </div>
    );

  }

 }


 export default Admin;