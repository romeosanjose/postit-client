import React, {Component} from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';

class ForgotPassword extends Component{

    constructor(props){
        super(props)
        this.state = {
            email: null,
            api_url: process.env.REACT_APP_API_URL,
        }
    }

    handleChange = (event) => {
        const input = event.target;
        this.setState({ email : input.value });
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        const data = {
            email : this.state.email           
        }
        console.log(this.props.match.params.id);
        const post = axios.post(this.state.api_url + '/password/forgot', data)
            .then(res => {
                localStorage.clear();
                this.props.history.push('/login');
            })
            .catch(error => {
                this.handleCatch(error);
            })
    }

    handleCatch = (error) => {
        if (error.response) {
            console.log(error.response.data)
            const errors = error.response.data.map((item, key)=>
                <p>{item}<br/></p>
            ); 
            this.setState({message : errors});   
        } else if (error.request) {
            console.log('request error', error.request);
            this.setState({message : 'Its not you, its me'});    
        } else {
            console.log('error message', error.message);
            this.setState({message : 'Its not you, its me'});    
        }
    }

    render(){
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-xl-10 col-lg-12 col-md-9">
                        <div className="card o-hidden border-0 shadow-lg my-5">
                            <div className="card-body p-0">
                                <div className="row">
                                    <div className="col-lg-6 d-none d-lg-block bg-login-image"></div>
                                        <div className="col-lg-6">
                                            <div className="p-5">
                                                <div className="text-center">
                                                    <h1 className="h4 text-gray-900 mb-4">Enter Email for Password Change</h1>
                                                </div>
                                                <form className="user" onSubmit={this.handleFormSubmit}>
                                                    <div className="form-group">
                                                        <input type="email" className="form-control form-control-user" name="email" value={this.state.email} id="email" onChange={this.handleChange} placeholder="Email" />
                                                    </div>
                                                    <button type="submit" className="btn btn-primary btn-user btn-block">Verify Email</button>
                                                    
                                                    <div name="message" style={{color:'red', marginTop: '2em', marginLeft: '7em'}}>{this.state.message}</div>
                                                </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        );
    }
}

export default ForgotPassword;