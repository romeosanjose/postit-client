import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import ReactDOM from 'react-dom';
import {withRouter} from 'react-router-dom';

class AdminLogout extends Component {

    constructor(props){
        super(props)
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout(){
        localStorage.clear();
        this.props.history.push('/login');
    }

    render(){
        return (
                <Link className="collapse-item" onClick={this.handleLogout}>
                    <i className="fas fa-fw fa-arrow-right"></i>
                    <span> Sign-out</span>
                </Link>   
        )
    }
}

export default withRouter(AdminLogout);