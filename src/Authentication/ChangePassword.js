import React, {Component} from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';
import FormComponent from '../FormComponent';

class ChangePassword extends FormComponent{

    constructor(props){
        super(props)
        this.state = {
            user_id : null,
            authentication : {
                password: null,
                password_repeat : null
            },
            api_url: process.env.REACT_APP_API_URL,
        }
    }

    handleChange = (event) => {
        const input = event.target;
        const authentication  = this.state.authentication;
        authentication[input.name] = input.value;
        this.setState({ authentication : authentication });
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        const data = {
            password : this.state.authentication           
        }
        const post = axios.post(this.state.api_url + '/password/change/' + this.props.match.params.id, data)
            .then(res => {
                localStorage.clear();
                this.props.history.push('/login');
            })
            .catch(error => {
                this.handleCatch(error);
            })
    }

    render(){
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-xl-10 col-lg-12 col-md-9">
                        <div className="card o-hidden border-0 shadow-lg my-5">
                            <div className="card-body p-0">
                                <div className="row">
                                    <div className="col-lg-6 d-none d-lg-block bg-login-image"></div>
                                        <div className="col-lg-6">
                                            <div className="p-5">
                                                <div className="text-center">
                                                    <h1 className="h4 text-gray-900 mb-4">Change Password</h1>
                                                </div>
                                                <form className="user" onSubmit={this.handleFormSubmit}>
                                                    <div className="form-group">
                                                        <input type="password" className="form-control form-control-user" name="password" value={this.state.password} id="password" onChange={this.handleChange} placeholder="Password" />
                                                    </div>
                                                    <div className="form-group">
                                                        <input type="password" className="form-control form-control-user" name="password_repeat" value={this.state.password_repeat} id="password_repeat" onChange={this.handleChange} placeholder="Repeat Password" />
                                                    </div>
                                                    <button type="submit" className="btn btn-primary btn-user btn-block">Change Password</button>
                                                    
                                                    <div name="message" style={{color:'red', marginTop: '2em', marginLeft: '7em'}}>{this.state.message}</div>
                                                </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        );
    }
}

export default ChangePassword;