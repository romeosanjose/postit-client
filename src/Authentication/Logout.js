import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import ReactDOM from 'react-dom';
import {withRouter} from 'react-router-dom';

class Logout extends Component {

    constructor(props){
        super(props)
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout(){
        localStorage.clear();
        this.props.history.push('/login');
    }

    render(){
        return (
                <Link className="w3-bar-item w3-button" onClick={this.handleLogout}>
                    Logout
                </Link>   
        )
    }
}

export default withRouter(Logout);