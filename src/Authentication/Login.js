import React, {Component} from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';
import {withRouter} from 'react-router-dom';
import {Link} from 'react-router-dom';

class Login extends Component{

    constructor(props) {
        super(props);
        
        this.state = {
            email : null,
            password: null,
            user: null,
            message : '',
            api_url: process.env.REACT_APP_API_URL
        }
    }

    handleChange = (event) => {
        const input = event.target;
        this.setState({[input.name] : input.value })
    };

    handleFormSubmit = (event) => {
        event.preventDefault();
        console.log(this.state.api_url);
        const data = {
            login : {
                'email' : this.state.email,
                'password' : this.state.password
            }
        };
        
        axios.post( this.state.api_url +  "/login", data)
        .then(res => {
            localStorage.setItem('user', JSON.stringify(res.data));
            this.props.history.push('/');
        })
        .catch(error => {
            if (error.response) {
                this.setState({message : error.response.data});    
            } else if (error.request) {
                this.setState({message : 'Its not you, its me'});    
            } else {
                this.setState({message : 'Its not you, its me'});    
            }
        });
    }

    render(){
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-xl-10 col-lg-12 col-md-9">
                        <div className="card o-hidden border-0 shadow-lg my-5">
                            <div className="card-body p-0">
                                <div className="row">
                                    <div className="col-lg-6 d-none d-lg-block bg-login-image"></div>
                                        <div className="col-lg-6">
                                            <div className="p-5">
                                                <div className="text-center">
                                                    <h1 className="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                                </div>
                                                <form className="user" onSubmit={this.handleFormSubmit}>
                                                    <div className="form-group">
                                                        <input type="text" className="form-control form-control-user" name="email" value={this.state.email} onChange={this.handleChange} id="email"  placeholder="Enter Email..." />
                                                    </div>
                                                    <div className="form-group">
                                                        <input type="password" className="form-control form-control-user" name="password" value={this.state.password} id="password" onChange={this.handleChange} placeholder="Password" />
                                                    </div>
                                                    <button type="submit" className="btn btn-primary btn-user btn-block">Login</button>
                                                    <Link to={{ pathname: "/password/forgot/" ,  state : {user : this.state.user}}} className="collapse-item" title="Forgot Password" >
                                                        <i className="fas fa-fw fa-user-shield"></i> 
                                                        <span > Forgot Password</span>
                                                    </Link>
                                                    <div name="message" style={{color:'red', marginTop: '2em', marginLeft: '7em'}}>{this.state.message}</div>
                                                </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}


export default Login

