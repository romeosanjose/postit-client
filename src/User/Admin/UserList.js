import React, {Component} from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';
import UserItem from './UserItem';
import SideBar from '../../NavBar/Admin/SideBar';

class Userlist extends Component{

   constructor(props) {
        super(props);
        let currentUser = JSON.parse(localStorage.getItem('user'));
        this.state = {
            originalUsers : null,
            users: null,
            displayedUsers : null,
            currentUser : currentUser,
            api_url: process.env.REACT_APP_API_URL,
        }
    }

    options() {
        return {
            headers : {
                'api-key' : (this.state.currentUser) ? this.state.currentUser.api_key : null
            }
        };
    }

  
   async componentDidMount(){     
        let users = (await axios.get(this.state.api_url + '/user', this.options())).data;
        this.setState({
            users: users,
            originalUsers : users,
            displayedUsers : users
        });
    }

    handleSearch =  (event) => {
        let searcjQery = event.target.value.toLowerCase()
        if (searcjQery === ''){
            this.setState({
                displayedUsers: this.state.originalUsers
            })
            return;    
        }

        let displayedUsers = this.state.users.filter((user) => {
                user.name = user.person.first_name + ' ' + user.person.last_name;
                let searchValue = user.name.toString().toLowerCase();
              return searchValue.indexOf(searcjQery) !== -1;
            })
        this.setState({
            displayedUsers: displayedUsers
        })
    }

    addButton() {
        if (this.state.currentUser.type === 'SUPERADMIN') {
            return <Link to="/adduser" >Add User</Link>;
        }
    }
   
    loadList(){
       let users = this.state.displayedUsers;
       return (
           <div className="col-lg-12">
                <div className="p-5">
                    <div className="text-center">
                        <h1 className="h4 text-gray-900 mb-4">User List</h1>
                    </div>
                    <div className="form-group">
                        <input type="text" className="form-control col-sm-6" onChange={this.handleSearch} placeholder="search..." />
                    </div>
                    {users === null && <img clasName="spinner" src="../img/spinner.gif" style={{marginLeft:"30%", maxHeight:"200px", maxWidth: "200px", height: "auto", width: "auto"}}/>}
                    {
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            {
                                users && users.map((user, index) => (
                                    <UserItem user={user} index={index} />
                                ))
                            }
                        </table>
                    }
                    {/* <ul className="list-group list-group-flush">
                        {
                            users && users.map((user, index) => (
                                <UserItem user={user} index={index}  />
                            ))
                        }
                    </ul> */}
                    
                </div>
            </div>
       );
   }

   render(){
    return(
        <div>
            {/* Page Wrapper */}
            <div id="wrapper">
                <SideBar />
                {/* Content Wrapper */}
                <div id="content-wrapper" className="d-flex flex-column">
                    {/* Main Content */}
                    <div id="content">
                        {this.loadList()}
                    {/* End of main Content */}
                    </div>
                {/* End of Content Wrapper */}
                </div>
            {/* End - Page Wrapper */}
            </div>  
        </div>
    );
}

}

export default Userlist;