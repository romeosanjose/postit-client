import React, {Component} from 'react';
import FormComponent from '../../FormComponent';
import ReactDOM from 'react-dom';
import axios from 'axios';
import SideBar from '../../NavBar/Admin/SideBar';

class AddUser extends FormComponent{

    constructor(props) {
        super(props);
        let currentUser = JSON.parse(localStorage.getItem('user'));
        let user = {
            email : '',
            person: {
                first_name : '',
                last_name : '',
                shift_color: '3',
                clinical_department : '5',
                year_level : '9',
                file : null,    
            },
            type: 'ADMIN'
        }
        
        this.state = {
            currentUser : currentUser,
            api_url: process.env.REACT_APP_API_URL,
            user : user, 
            message: "",
            file : '',
            imagePreviewUrl: '',
            shift_color_opts : [],
            clinical_dept_opts : [],
            year_level_opts : [],
        }
    }

    options(){
        return { 
            headers : { 
                'api-key' : this.state.currentUser.api_key
            }
        }
    }

     handleChange = (event) => {
        const input = event.target;
        const user  = this.state.user;
        if (input.name === 'first_name' || input.name === 'last_name') {
            user.person[input.name] = input.value;
        } else {
            user[input.name] = input.value;
        }
        this.setState({ user : user })
    };

    handleTypeSelect = (event) => {
        const type = event.target.value;
        const user = this.state.user;
        user.type = type;
        this.setState({
            user : user
        });
    }

    handlePersonListSelect = (event) => {
        const select = event.target;
        const user = this.state.user
        const person = user.person
        person[select.name] = select.value
        user.person = person
        this.setState({
            user : user
        })
        console.log(this.state.user)
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        const data = {
            user : this.state.user,
            file: this.state.file
        }
        
        const post = axios.post(this.state.api_url + '/user', data, this.options())
            .then(res => {
                this.props.history.push('/admin/user');
            })
            .catch(error => {
                this.handleCatch(error);
            })
    }

    handleImageChange = (e) => {
        
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
               
        var formData = new FormData();
        formData.append('file', e.target.files[0]);
        const post = axios.post(this.state.api_url + '/file', formData, {
            headers : {
                'api-key' : this.state.currentUser.api_key,
                'Content-Type' : 'multipart/form-data'
            }
        })
            .then(res => {
                console.log(res.data);
                let user = this.state.user;
                user.person.file = res.data; 
                this.setState({
                     user : user   
                });
            })
            .catch(error => {
                this.handleCatch(error);
            })

    }

    uploadForm() {
        return (
            <div  className="form-group upload-btn-wrapper">
                <input type="file" id="file" onChange={(e)=>this.handleImageChange(e)} />
                <label className="uploadbtn">Upload Picture</label>
            </div>
        );
    }
    

    componentDidMount() {
        this.loadShiftColor();
        this.loadDept();
        this.loadYear();
    }

    loadShiftColor() {
        const color = axios.get(this.state.api_url + '/managelist/SHIFT_COLOR', this.options())
        .then(res => {
            this.setState({shift_color_opts : res.data})
        }).catch(error => {
            this.handleCatch(error);
        })
    }


    loadDept() {
        const color = axios.get(this.state.api_url + '/managelist/CLINICAL_DEPT', this.options())
        .then(res => {
            this.setState({clinical_dept_opts : res.data})
        }).catch(error => {
            this.handleCatch(error);
        })
    }

    loadYear() {
        const color = axios.get(this.state.api_url + '/managelist/YEAR_LEVEL', this.options())
        .then(res => {
            this.setState({year_level_opts : res.data})
        }).catch(error => {
            this.handleCatch(error);
        })
    }

    loadForm(){
        let avatar = (this.state.user && this.state.user.person.profile_picture) 
        ? this.state.user.person.profile_picture.path
        : '/profile_picture/avatar.jpg';
        return (
            <div>
                <div className="card o-hidden border-0 shadow-lg my-5">
                    <div className="card-body p-0">    
                        <div className="row">               
                            <div className="col-lg-5 d-none d-lg-block bg-register-image"></div>
                            <div className="col-lg-7">
                                <div className="p-5">
                                    <div className="text-center">
                                        <h1 className="h4 text-gray-900 mb-4">Create an Account!</h1>
                                    </div>
                                    <form onSubmit={this.handleFormSubmit} className="user">
                                        <div name="message" class="error-message">{this.state.message}</div>
                                        <div className="text-center">
                                            <div className="form-group">
                                                <img src={ this.state.api_url + '/' +  avatar } class="w3-circle" style={{ height:"106px", width:"106px"}} alt="Avatar" />
                                            </div>
                                            {this.uploadForm()}
                                        </div>
                                        <div className="form-group">
                                            <input name="email" id="email" className="form-control form-control-user"
                                            value={this.state.user.email} onChange={this.handleChange} placeholder="email *" required/>
                                            
                                        </div>
                                        <div className="form-group">
                                            <span style={{color:'grey', fontSize: '.8em'}}>* Role</span> 
                                            <select onChange={this.handleTypeSelect} name="type" id="type" className="select-css" required>
                                                <option value="ADMIN">Admin</option>
                                                <option value="PUBLISHER">Publisher</option>
                                            </select>
                                        </div>
                                        <div id="user-details">
                                            <div className="form-group">
                                                <input name="first_name" id="first_name" className="form-control form-control-user"
                                                value={this.state.user.person.first_name} onChange={this.handleChange} placeholder="firstname *" required/>
                                            </div>
                                            <div className="form-group">
                                                <input name="last_name" id="last_name" className="form-control form-control-user"
                                                value={this.state.user.person.last_name} onChange={this.handleChange} placeholder="lastname *" required/>
                                            </div>
                                            <div className="form-group">
                                                <span style={{color:'grey', fontSize: '.8em'}}>* shift color</span> 
                                                <select onChange={this.handlePersonListSelect} name="shift_color" id="shift_color" className="select-css" required>
                                                {
                                                    this.state.shift_color_opts.map((color)=> <option key={color.id} value={color.id}>{color.name}</option>)
                                                }
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <span style={{color:'grey', fontSize: '.8em'}}>* clinical department</span> 
                                                <select onChange={this.handlePersonListSelect} name="clinical_department" id="clinical_department" className="select-css" required>
                                                {
                                                    this.state.clinical_dept_opts.map((dept)=> <option key={dept.id} value={dept.id}>{dept.name}</option>)
                                                }
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <span style={{color:'grey', fontSize: '.8em'}}>* year level</span> 
                                                <select onChange={this.handlePersonListSelect} name="year_level" id="year_level" className="select-css" required>
                                                {
                                                    this.state.year_level_opts.map((year)=> <option key={year.id} value={year.id}>{year.name}</option>)
                                                }
                                                </select>
                                            </div>                                         
                                        </div>
                                        <div >
                                            <button className="btn btn-facebook btn-user btn-block" type="submit">save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        );
    }

    
}

export default AddUser;