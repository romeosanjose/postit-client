import React, {Component} from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';
import SideBar from '../../NavBar/Admin/SideBar';
import AddUser from './AddUser';

class EditUser extends AddUser{

    constructor(props) {
        super(props);
        let currentUser = JSON.parse(localStorage.getItem('user'));
        var changeUser = props.location.state.user;
        var changeFile = props.location.state.user.person.profile_picture;
        changeUser.person.file = changeFile;
        changeUser.person.shift_color = changeUser.person.shift_color.id
        changeUser.person.clinical_department = changeUser.person.clinical_department.id
        changeUser.person.year_level = changeUser.person.year_level.id
        delete changeUser.email
        this.state = {
            currentUser : currentUser,
            api_url: process.env.REACT_APP_API_URL,
            user : changeUser, 
            message: "",
            file : '',
            imagePreviewUrl: '',
            isFileChanged : false,
            isAdmin : (currentUser.type === 'ADMIN') ? true : false,
            isEditAllow : (currentUser.type === 'ADMIN' || currentUser.id === changeUser.id) ? true : false,
            shift_color_opts : [],
            clinical_dept_opts : [],
            year_level_opts : [],
        }

        console.log(this.state);

    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        var data = {};        
        if (this.state.isFileChanged){
            data =  {
                user : this.state.user,
                file: this.state.file
            } 
        } else {
            var user = this.state.user;
            user.person.file = null
            data = {
                user : user
            }            
        }
        
        
        const post = axios.patch(this.state.api_url + '/user/' + this.state.user.uuid, data, this.options())
            .then(res => {
                this.props.history.push('/admin/user');
            })
            .catch(error => {
                this.handleCatch(error);
            })
    }

    showUpload(){
        return (this.state.isEditAllow) ? this.uploadForm() : null;
    }

    showSubmit(){
        return (this.state.isEditAllow) 
            ?   <div >
                    <button className="btn btn-facebook btn-user btn-block" type="submit">save</button>
                </div>
            :   null;
    }

    
    loadForm(){
        let avatar = (this.state.user && this.state.user.person.profile_picture) 
        ? this.state.user.person.profile_picture.path
        : '/profile_picture/avatar.jpg';

        let disabled = (this.state.isEditAllow) ? "" : "disabled";

        let userTypeSelection = (this.state.isAdmin) 
                    ?  <div className="form-group">
                            <span style={{color:'grey', fontSize: '.8em'}}>* Role</span> 
                            <select onChange={this.handleTypeSelect} name="type" id="type" className="select-css" value={this.state.user.type} disabled={disabled} required>
                                <option value="ADMIN">Admin</option>
                                <option value="PUBLISHER">Publisher</option>
                            </select> 
                        </div>
                    : null;
        let shiftColorSelection = (this.state.user.person.shift_color)
                    ?  <div className="form-group">
                            <span style={{color:'grey', fontSize: '.8em'}}>* Shift Color</span> 
                            <select onChange={this.handlePersonListSelect} name="shift_color" id="shift_color" className="select-css" value={this.state.user.person.shift_color} disabled={disabled} required>
                            {
                                this.state.shift_color_opts.map((color)=> <option key={color.id} value={color.id}>{color.name}</option>)
                            }
                            </select>
                        </div>
                    : null;
        let clinicalDepartmentSelection = (this.state.user.person.clinical_department)
                    ?  <div className="form-group">
                            <span style={{color:'grey', fontSize: '.8em'}}>* Clinical Department</span> 
                            <select onChange={this.handlePersonListSelect} name="clinical_department" id="clinical_department" className="select-css" value={this.state.user.person.clinical_department} disabled={disabled} required>
                            {
                                this.state.clinical_dept_opts.map((dept)=> <option key={dept.id} value={dept.id}>{dept.name}</option>)
                            }
                            </select>
                        </div>
                    : null;
        let yearLevelSelection = (this.state.user.person.year_level)
                    ?  <div className="form-group">
                            <span style={{color:'grey', fontSize: '.8em'}}>* Clinical Department</span> 
                            <select onChange={this.handlePersonListSelect} name="year_level" id="year_level" className="select-css" value={this.state.user.person.year_level} disabled={disabled} required>
                            {
                                this.state.year_level_opts.map((year)=> <option key={year.id} value={year.id}>{year.name}</option>)
                            }
                            </select>
                        </div>
                    : null;
       
        return (
            
                <div>
                    <div className="card o-hidden border-0 shadow-lg my-5">
                        <div className="card-body p-0">    
                            <div className="row">               
                                <div className="col-lg-5 d-none d-lg-block bg-register-image"></div>
                                <div className="col-lg-7">
                                    <div className="p-5">
                                        <div className="text-center">
                                            <h1 className="h4 text-gray-900 mb-4">Create an Account!</h1>
                                        </div>
                                        <form onSubmit={this.handleFormSubmit} className="user">
                                            <div name="message" class="error-message">{this.state.message}</div>
                                            <div className="text-center">
                                                <div className="form-group">
                                                    <img src={ this.state.api_url + '/' +  avatar } class="w3-circle" style={{ height:"106px", width:"106px"}} alt="Avatar" />
                                                </div>
                                                {this.showUpload()}
                                            </div>
                                            {userTypeSelection}
                                            <div id="user-details">
                                                <div className="form-group">
                                                    <input name="first_name" id="first_name" className="form-control form-control-user"
                                                    value={this.state.user.person.first_name} onChange={this.handleChange} placeholder="firstname *" disabled={disabled} required/>
                                                </div>
                                                <div className="form-group">
                                                    <input name="last_name" id="last_name" className="form-control form-control-user"
                                                    value={this.state.user.person.last_name} onChange={this.handleChange} placeholder="lastname *" disabled={disabled} required/>
                                                </div>
                                                {shiftColorSelection}
                                                {clinicalDepartmentSelection}
                                                {yearLevelSelection}                                      
                                            </div>
                                            {this.showSubmit()}
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
                
        );
    }

    

}

export default EditUser;