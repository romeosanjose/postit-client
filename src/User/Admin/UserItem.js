import React, {Component} from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';

class UserItem extends Component{
    
    constructor(props) {
        super(props);
        let currentUser = JSON.parse(localStorage.getItem('user'));
        this.state = {
            currentUser : currentUser,
            api_url: process.env.REACT_APP_API_URL
        };
    }

    options() {
        return {
            headers : {
                'api-key' : (this.state.currentUser) ? this.state.currentUser.api_key : null
            }
        };
    }

    followers(user) {
        return (<span className="followers" style={{fontStyle:'Italic'}}>followers: {user.followers.length}</span>);
    }

    following(user) {
        return (<span className="following" style={{fontStyle:'Italic'}}>following: {user.following.length}</span>);
    }

    followButton(user) {

        if (user.uuid === this.state.currentUser.uuid) {
            return;
        }

        if (user.followers.includes(this.state.currentUser.id)) {
            return  <button className="btn btn-facebook" onClick={() => this.handleFollow(user.uuid, 'unfollow')}>
                        unfollow
                    </button>
        }

        return <button className="btn btn-facebook" onClick={() => this.handleFollow(user.uuid, 'follow')}>
                    follow
                </button>
    }

    handleFollow(uuid, follow){
        const followUuid = {
            user : uuid
        }
        
        axios.patch(this.state.api_url + '/user/' + this.state.currentUser.uuid + '/' + follow, followUuid , this.options())
        .then(res => {
            if (res.data) {
                this.refreshUser(uuid)
            }
        });
    
    }

    handleUnlock (user)  {
        console.log('dasdasdasd');
        axios.post(this.state.api_url + '/user/' + user.id + '/unlock',null, this.options())
        .then(res => {
            if (res.data) {
                this.refreshUser(user.uuid)
            }
        });
    }

    showUnlockButton(user) {
        return (user.locked)
            ? <button className="btn btn-facebook" onClick={() => this.handleUnlock(user)}>unlock </button>
            : null;
    }

    refreshUser(uuid) {
        axios.get(this.state.api_url + '/user/' + uuid, this.options())
        .then(res => {
            let user = res.data;
            let element = this.buildUserDetailElement(user);
            ReactDOM.render(element, document.getElementById('user-detail-' + user.uuid));
        });
    }

    buildUserElement(user){
        let btnIconClass = (this.state.currentUser.uuid === user.uuid || this.state.currentUser.type === "ADMIN")
                        ? "fas fa-edit" : "fas fa-eye";
        let status = user.locked ? 'Locked' : 'Active';                        
        return  <tr>
                    <td>{user.id}</td>
                    <td>{user.person.first_name}</td>
                    <td>{user.person.last_name}</td>
                    <td>{user.type}</td>
                    <td>{status}</td>
                    <td>
                        <Link to={{ pathname: "/admin/user/edit",  state : {user : user}}} className="btn btn-facebook" >
                            <i className={btnIconClass}></i>
                        </Link>
                        {this.showUnlockButton(user)}
                    </td>
                </tr>
    }

    buildUserDetailElement(user){
        return (
                <div id={'user-detail-' + user.uuid}>
                    <div className="list-item-component" id={'user-name-' + user.uuid}>{user.person.first_name + ' ' + user.person.last_name}</div>
                </div>
        );
    }

    render(){
        let user = this.props.user;
        let index = this.props.index;
        return (
            <tbody>
                {this.buildUserElement(user)}
            </tbody>
        );
    }
}

export default UserItem;