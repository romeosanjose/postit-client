import React, {Component} from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';


class MyProfile extends Component{

    constructor(props) {
        super(props);
        let user = JSON.parse(localStorage.getItem('user'));
        this.state = {
            user : user,
            api_url: process.env.REACT_APP_API_URL,
        };
    }

    showProfileSetting() {
        return (this.state.user.id > 1) 
            ?
            <div> 
                <p><i class="fa fa-palette fa-fw w3-margin-right w3-text-theme"></i>{this.state.user.person.shift_color.name}</p>
                <p><i class="fas fa fa-building fa-fw w3-margin-right w3-text-theme"></i>{this.state.user.person.clinical_department.name}</p>
            </div>
            : null
    }

    render() {

        let avatar = (this.state.user && this.state.user.person.profile_picture) 
        ? this.state.user.person.profile_picture.path
        : '/profile_picture/avatar.jpg';
        return (
            <div class="w3-card w3-round w3-white">
                <div class="w3-container">
                    <h4 class="w3-center">My Profile</h4>
                    <p class="w3-center">
                        <img src={ this.state.api_url + '/' +  avatar } class="w3-circle" style={{ height:"106px", width:"106px"}} alt="Avatar" />
                    </p>
                    <hr></hr>
                    <p><i class="fa fa-rocket fa-fw w3-margin-right w3-text-theme"></i> {this.state.user.person.first_name + ' ' + this.state.user.person.last_name }</p>
                    <p><i class="fa fa-envelope fa-fw w3-margin-right w3-text-theme"></i>{this.state.user.email}</p>
                    {this.showProfileSetting()}
                </div>
            </div>
        );
    }
}

export default MyProfile;