import React, {Component} from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';
import NavBar from '../NavBar/NavBar';
import {Link} from 'react-router-dom';
import TreatmentItem from './TreatmentItem';

class Treatments extends Component{

   constructor(props) {
        super(props);
        let user = JSON.parse(localStorage.getItem('user'));
        this.state = {
            treatments: null,
            originalTreatments : null,
            user : user,
            api_url: process.env.REACT_APP_API_URL,
        }
    }

    options() {
        return {
            headers : {
                'api-key' : (this.state.user) ? this.state.user.api_key : null
            }
        };
    }

    async componentDidMount(){     
        this.loadCaseJobTreatment();
     }

     loadCaseJobTreatment() {
        const treatment = axios.get(this.state.api_url + '/managelist/TREATMENT', this.options())
        .then(res => {
            console.log(res.data);
            this.setState({
                treatments : res.data,
                originalTreatments : res.data
            })
        }).catch(error => {
            this.handleCatch(error);
        })
    }
 
     handleSearch =  (event) => {
        let searcjQery = event.target.value.toLowerCase()
        if (searcjQery === ''){
            this.setState({
                treatments: this.state.originalTreatments
            })
            return;    
        }
        let displayedTreatments = this.state.treatments.filter((treatment) => {
                 let searchValue = treatment.name.toString().toLowerCase();
               return searchValue.indexOf(searcjQery) !== -1;
             }) 
         this.setState({
            treatments: displayedTreatments
         });
        
     }
 
    
    render(){
        let treatments = this.state.treatments;
        return (
            <div className="w3-card w3-round w3-white w3-hide-small">
                    <div className="w3-row-padding">
                        <div className="w3-col m12">
                            <div className="w3-container ">
                                <input type="text" className="form-control col-sm-12" onChange={this.handleSearch} placeholder="search..." />
                            </div>
                        </div>
                    </div>
                    {treatments === null && <img clasName="spinner" src="../img/spinner.gif" style={{marginLeft:"30%", maxHeight:"200px", maxWidth: "200px", height: "auto", width: "auto"}}/>} 
                    <div className="w3-container">
                        {
                            treatments && treatments.map(treatment => (  
                                <TreatmentItem treatment={treatment} />
                            ))
                        }
                    </div>
             </div>
             
        );
    }
}

export default Treatments;