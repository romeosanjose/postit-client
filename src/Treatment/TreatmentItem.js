
import React, {Component} from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';
import NavBar from '../NavBar/NavBar';
import {Link} from 'react-router-dom';
import Posts from '../Posts/Posts';

class TreatmentItem extends Posts{
    
    constructor(props) {
        super(props);
        let user = JSON.parse(localStorage.getItem('user'));
        this.state = {
            user : user,
            created_by :  null,
            api_url: process.env.REACT_APP_API_URL,
            treatment : this.props.treatment
        };
       
    }

    options() {
        return {
            headers : {
                'api-key' : (this.state.user) ? this.state.user.api_key : null
            }
        };
    }

    handleTreatmentClick(id){
       const posts = (axios.get(this.state.api_url + '/post/treatment/' + id, this.options())).data;
       this.updatePosts(posts);
    }

    render(){
        let treatment = this.state.treatment;
        let theme = "w3-tag w3-small w3-theme-d" + Math.floor(Math.random() * 6);
        return (
            <span id={'treatment-' + treatment.id}  className={theme} onClick={() => this.handleTreatmentClick(treatment.id)}>
                {treatment.name}
            </span>
            
        );
    }
}


export default TreatmentItem;