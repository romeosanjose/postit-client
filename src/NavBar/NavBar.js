import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import ReactDOM from 'react-dom';
import {withRouter} from 'react-router-dom';
import Logout from '../Authentication/Logout';


class NavBar extends Component {

    constructor(props){
        super(props);
        if (!localStorage.getItem('user')) {
            this.props.history.push('/login');
        } 
        let user = JSON.parse(localStorage.getItem('user'));
        let userFirstname = (user && user.person.first_name) ? user.person.first_name : 'There';
        this.state = {
            user : user,
            api_url: process.env.REACT_APP_API_URL,
            user_firstname : userFirstname,
        };
    }

    render(){

        let avatar = (this.state.user && this.state.user.person.profile_picture) 
        ? this.state.user.person.profile_picture.path
        : '/profile_picture/avatar.jpg';
        return (
             <div className="w3-top">
                <div className="w3-bar w3-theme-d2 w3-left-align w3-large">
                    <a className="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
                    <a href="/" className="w3-bar-item w3-button w3-padding-large w3-theme-d4"><img src={this.state.api_url + "/logo.png"} style={{height:30}} alt="PUCA" />UCA</a>
                    
                    <Link to={{ pathname: "/admin/user/edit",  state : {user : this.state.user}}}  className="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white" title="My Account" >
                        <img src={this.state.api_url + '/' +  avatar} className="w3-circle" style={{height:"23px", width:"23px"}} alt="Avatar" />   
                    </Link>
                    <div className="w3-dropdown-hover w3-right">
                        <button className="w3-button w3-padding-large" title="Account">Hi {this.state.user_firstname}</button>     
                        <div className="w3-dropdown-content w3-card-4 w3-bar-block "  style={{width:"300ppx"}}>
                            <Logout />
                        </div>
                    </div> 
                </div>
            </div>    
        );
    }
}

export default withRouter(NavBar)