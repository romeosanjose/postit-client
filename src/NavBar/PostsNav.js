import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import ReactDOM from 'react-dom';
import {withRouter} from 'react-router-dom';

class PostsNav extends Component {

    constructor(props){
        super(props)
    }

    handleLogout(){
        this.props.history.push('/posts');
    }

    render(){
        return (
                <Link className="navbar-left" onClick={this.handleLogout}>
                    Posts
                </Link>   
        )
    }
}

export default withRouter(PostsNav);