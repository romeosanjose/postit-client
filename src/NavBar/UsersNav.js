import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import ReactDOM from 'react-dom';
import {withRouter} from 'react-router-dom';

class UsersNav extends Component {

    constructor(props){
        super(props)
    }

    handleLogout(){
        this.props.history.push('/users');
    }

    render(){
        return (
                <Link className="navbar-left" onClick={this.handleLogout}>
                    Users
                </Link>   
        )
    }
}

export default withRouter(UsersNav);