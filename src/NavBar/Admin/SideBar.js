import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import ReactDOM from 'react-dom';
import {withRouter} from 'react-router-dom';
import AdminLogout from '../../Authentication/AdminLogout';



class SideBar extends Component {

    constructor(props){
        super(props);
        if (!localStorage.getItem('user')) {
             this.props.history.push('/login');
        } 
        let user = JSON.parse(localStorage.getItem('user'));
        let userFirstname = (user && user.person.first_name) ? user.person.first_name : 'There';
        this.state = {
            user : user,
            api_url: process.env.REACT_APP_API_URL,
            component : null,
            user_firstname : userFirstname,
        };
    }

    render(){
        let userAdd = (this.state.user.type === 'ADMIN')
            ? <a className="collapse-item" href="/admin/user/add">
                <i className="fas fa-fw fa-user-plus"></i>
                <span> Add User</span>
             </a>
            : null;

   
        return (
            <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
                {/* Sidebar - Brand */}
                <a className="sidebar-brand d-flex align-items-center justify-content-center" href="/admin">
                    <div className="sidebar-brand-text mx-3">
                        <img src={this.state.api_url + "/logo.png"} style={{height:30}} alt="PUCA" /><span style={{color:'white'}}>UCA</span>
                    </div>
                {/* End of Sidebar - brand */}
                </a>

                {/* Divider */}
                <hr className="sidebar-divider my-0" />
               
                {/* Nav Item - Dashboard  */}               
                <li className="nav-item active">               
                    <a className="nav-link" href="/admin">               
                        <i className="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                {/* Divider */}
                <hr className="sidebar-divider"></hr>

                {/* Heading */}
                <div className="sidebar-heading">Interface</div>

                {/* Nav Item - Pages Collapse Menu */}
                <li className="nav-item">
                    <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsers" aria-expanded="true" aria-controls="collapseUsers">
                        <i className="fas fa-fw fa-users"></i>
                        <span>Users</span>
                    </a>
                    <div id="collapseUsers" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div className="bg-white py-2 collapse-inner rounded">
                        {userAdd}
                        <a className="collapse-item" href="/admin/user">
                            <i className="fas fa-fw fa-users"></i>
                            <span> User List</span>
                        </a>
                        </div>
                    </div>
                </li>


                <li className="nav-item">
                    <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePost" aria-expanded="true" aria-controls="collapsePost">
                        <i className="fas fa-fw fa-rocket"></i>
                        <span>Posts</span>
                    </a>
                    <div id="collapsePost" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div className="bg-white py-2 collapse-inner rounded">
                            <a className="collapse-item" href="/admin/post/add">
                                <i className="fas fa-fw fa-plus-square"></i>
                                <span> Add Post</span>
                            </a>
                        </div>
                    </div>
                </li>

                <li className="nav-item">
                    <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSettings" aria-expanded="true" aria-controls="collapseSettings">
                        <i className="fas fa-fw fa-gear"></i>
                        <span>Settings</span>
                    </a>
                    <div id="collapseSettings" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div className="bg-white py-2 collapse-inner rounded">
                            <Link to={{ pathname: "/admin/user/edit",  state : {user : this.state.user}}} className="collapse-item" title="My Account" >
                                <i className="fas fa-fw fa-user-circle"></i> 
                                <span > My Account</span>
                            </Link>
                            <Link to={{ pathname: "/password/change/" + this.state.user.uuid,  state : {user : this.state.user}}} className="collapse-item" title="Change Password" >
                                <i className="fas fa-fw fa-user-shield"></i> 
                                <span > Change Password</span>
                            </Link>
                            <AdminLogout />
                        </div>
                    </div>

                    
                </li>

                <li className="nav-item">
                    <span style={{color:'white', fontSize:'1.5em', fontWeight:'bolder', padding:'1em'}}>{'Welcome ' + this.state.user_firstname + '!'}</span>
                </li>
                

            </ul>
        );
    }
}

export default SideBar;