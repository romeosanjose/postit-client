import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import ReactDOM from 'react-dom';
import {withRouter} from 'react-router-dom';
import Logout from '../../Authentication/Logout';


class SideToggleBar extends Component {

    constructor(props){
        super(props);
        if (!localStorage.getItem('user')) {
            this.props.history.push('/login');
        } 
        let user = JSON.parse(localStorage.getItem('user'));
        
        this.state = {
            user : user,
            api_url: process.env.REACT_APP_API_URL,
        };
    }

    render(){
        return (
            <div>
                {/* Topbar */}
                <nav className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                    {/* Sidebar Toggle (Topbar) */}
                    <button id="sidebarToggleTop" className="btn btn-link d-md-none rounded-circle mr-3">
                        <i className="fa fa-bars"></i>
                    </button>
                        {/* Topbar Search */}
                        <form className="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                        <div className="input-group">
                            <input type="text" className="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                            <div className="input-group-append">
                            <button className="btn btn-primary" type="button">
                                <i className="fas fa-search fa-sm"></i>
                            </button>
                            </div>
                        </div>
                        </form>
                </nav>  
            </div> 
        );
    }

}

export default SideToggleBar;